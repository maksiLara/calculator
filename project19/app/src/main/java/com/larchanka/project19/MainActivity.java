package com.larchanka.project19;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editTextNumberOne, editTextNumberTwo;
    Button buttonAmount, buttonSubtraction, buttonMultiplication, buttonDevision;
    TextView textViewResult;
    String oper = "";

    final int MENU_RESET_ID = 1;
    final int MENU_QUIT_ID = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextNumberOne = findViewById(R.id.etNum1);
        editTextNumberTwo = findViewById(R.id.etNum2);

        buttonAmount = findViewById(R.id.but1);
        buttonSubtraction = findViewById(R.id.but2);
        buttonMultiplication = findViewById(R.id.but3);
        buttonDevision = findViewById(R.id.but4);

        buttonAmount.setText("+");
        buttonSubtraction.setText("-");
        buttonMultiplication.setText("*");
        buttonDevision.setText("/");

        textViewResult = findViewById(R.id.tvResult);

        buttonAmount.setOnClickListener(this);
        buttonSubtraction.setOnClickListener(this);
        buttonMultiplication.setOnClickListener(this);
        buttonDevision.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        double num1 = 0;
        double num2 = 0;
        double result = 0;

        //Проверка!!! пусты ли наши поля
        if (TextUtils.isEmpty(editTextNumberOne.getText().toString()) || TextUtils.isEmpty(editTextNumberTwo.getText().toString())) {
            return;
        }

        //Заполняем переменные числами
        num1 = Double.parseDouble(editTextNumberOne.getText().toString());
        num2 = Double.parseDouble(editTextNumberTwo.getText().toString());

        switch (v.getId()) {
            case R.id.but1:     //Сумма
                oper = "+";
                result = num1 + num2;
                break;
            case R.id.but2:     //Вычетание
                oper = "-";
                result = num1 - num2;
                Toast.makeText(this, "Resulta - " + result, Toast.LENGTH_SHORT).show();
                break;
            case R.id.but3:     //Произведение
                oper = "*";
                result = num1 * num2;
                Toast.makeText(this, "Resulta - " + result, Toast.LENGTH_SHORT).show();
                break;
            case R.id.but4:     //Деление
                oper = "/";
                try {
                    result = num1 / num2;
                    if (num2 == 0.0) {
                        Toast.makeText(this, "Нельзя делить на ноль", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(this, "Result - " + result, Toast.LENGTH_SHORT).show();
                    }
                } catch (ArithmeticException e) {
                    Toast.makeText(this, "Нельзя делить на ноль", Toast.LENGTH_LONG).show();
                }
                break;
        }
        textViewResult.setText(num1 + " " + oper + " " + num2 + " = " + result);

    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        menu.add(0, MENU_RESET_ID, 0, "RESET");
        menu.add(0, MENU_QUIT_ID, 0, "QUIT");
        return super.onCreatePanelMenu(featureId, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_RESET_ID:
                editTextNumberTwo.setText("");
                editTextNumberOne.setText("");
                textViewResult.setText("");
                break;
            case MENU_QUIT_ID:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
